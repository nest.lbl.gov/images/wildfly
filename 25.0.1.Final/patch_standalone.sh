*** wildfly/bin/standalone.sh	1969-12-21 17:00:00.000000000 -0700
--- wildfly/bin/standalone.sh	1969-12-21 17:00:00.000000000 -0700
***************
*** 4,9 ****
--- 4,14 ----
  # Usage : standalone.sh --debug
  #         standalone.sh --debug 9797
  
+ # Set up local environement
+ if [ -r $JBOSS_HOME/extras/local.env ] ; then
+     . $JBOSS_HOME/extras/local.env
+ fi
+ 
  # By default debug mode is disabled.
  DEBUG_MODE="${DEBUG:-false}"
  DEBUG_PORT="${DEBUG_PORT:-8787}"
***************
*** 318,323 ****
--- 323,336 ----
    JAVA_OPTS="-javaagent:\"${JBOSS_HOME}/jboss-modules.jar\" ${JAVA_OPTS}"
  fi
  
+ JBOSS_TRUSTSTORE=""
+ if [ "X" != "X$JBOSS_CACERTS" ]; then
+     JBOSS_TRUSTSTORE="$JBOSS_TRUSTSTORE -Djavax.net.ssl.trustStore=\""$JBOSS_CACERTS"\""
+ fi
+ if [ "X" != "X$JBOSS_CACERTS_PASSWORD" ]; then
+     JBOSS_TRUSTSTORE="$JBOSS_TRUSTSTORE -Djavax.net.ssl.trustStorePassword=\""$JBOSS_CACERTS_PASSWORD"\""
+ fi
+ 
  # Display our environment
  echo "========================================================================="
  echo ""
***************
*** 344,349 ****
--- 357,363 ----
           org.jboss.as.standalone \
           -Djboss.home.dir=\""$JBOSS_HOME"\" \
           -Djboss.server.base.dir=\""$JBOSS_BASE_DIR"\" \
+          $JBOSS_TRUSTSTORE \
           "$SERVER_OPTS"
        JBOSS_STATUS=$?
     else
***************
*** 357,362 ****
--- 371,377 ----
           org.jboss.as.standalone \
           -Djboss.home.dir=\""$JBOSS_HOME"\" \
           -Djboss.server.base.dir=\""$JBOSS_BASE_DIR"\" \
+          $JBOSS_TRUSTSTORE \
           "$SERVER_OPTS" "&"
        JBOSS_PID=$!
        # Trap common signals and relay them to the jboss process
