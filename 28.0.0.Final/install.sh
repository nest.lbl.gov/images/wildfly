#!/bin/bash
#
# This file attempts to be the moral equivalent of the Dockerfile, expect
#   it is run to install Wildfly in a running system and installing everything
#   in the user's area.
#

if [ "X" == "X${GIT_WILDFLY_CLONE}" ] ; then
    echo "The path to the docker_images project must be specified in the GIT_WILDFLY_CLONE variable" >&2
    exit 1
fi

if [ "X" == "X${1}" ] ; then
    jdk=11
else
    jdk=${1}
    shift
fi

if [ "X" == "X${1}" ] ; then
    dist=centos7
else
    dist=${1}
    shift
fi

# Set the WILDFLY_VERSION env variable
WILDFLY_VERSION=28.0.0.Final
WILDFLY_SHA1=f46be038de68abbe878df578efb1b6b2043f6c91
JBOSS_HOME=${HOME}/wildfly
USER_HOME=${HOME}

# Set the local wildfly/jboss, LUX-Zeplin services and launch env variables in one line to minimize layer count
# LAUNCH_JBOSS_IN_BACKGROUND ensures signals are forwarded to the JVM process correctly for graceful shutdown
STANDALONE_WORKDIR=${JBOSS_HOME}/workdir/standalone
JBOSS_EXTRAS=${JBOSS_HOME}/workdir/extras
POSTGRES_JDBC_VERSION=42.6.0
MYSQL_CONNECTOR_VERSION=8.0.32


cd ${HOME}
curl --insecure -L -s -O https://github.com/wildfly/wildfly/releases/download/${WILDFLY_VERSION}/wildfly-${WILDFLY_VERSION}.tar.gz
case "`uname`" in
    Darwin*)
        shasum wildfly-${WILDFLY_VERSION}.tar.gz | grep ${WILDFLY_SHA1}
        result=${?}
        ;;

    *)
        sha1sum wildfly-${WILDFLY_VERSION}.tar.gz | grep ${WILDFLY_SHA1}
        result=${?}
        ;;

esac
if [ 0 != ${result} ] ; then
    exit 2
fi
tar -xf wildfly-${WILDFLY_VERSION}.tar.gz
rm -fr ${JBOSS_HOME}
mv wildfly-${WILDFLY_VERSION} ${JBOSS_HOME}
rm wildfly-${WILDFLY_VERSION}.tar.gz

curl --insecure -s -L -O https://jdbc.postgresql.org/download/postgresql-${POSTGRES_JDBC_VERSION}.jar
mv postgresql-${POSTGRES_JDBC_VERSION}.jar ${JBOSS_HOME}/standalone/deployments/
curl --insecure -s -L -O https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.tar.gz
tar zxvf mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.tar.gz \
    mysql-connector-j-${MYSQL_CONNECTOR_VERSION}/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar
mv mysql-connector-j-${MYSQL_CONNECTOR_VERSION}/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar \
    ${JBOSS_HOME}/standalone/deployments/
rmdir mysql-connector-j-${MYSQL_CONNECTOR_VERSION}
rm -f mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.tar.gz

(cd ${GIT_WILDFLY_CLONE}/${WILDFLY_VERSION} ; \
  cp patch_standalone.sh patch_TimeZoneMapping.properties patch_wildfly-init-redhat.sh patch_wildfly.conf ${JBOSS_HOME}/)

# This section adds in the MDT time zone
jar xf ${JBOSS_HOME}/standalone/deployments/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar com/mysql/cj/util/TimeZoneMapping.properties
patch com/mysql/cj/util/TimeZoneMapping.properties ${JBOSS_HOME}/patch_TimeZoneMapping.properties
jar uf ${JBOSS_HOME}/standalone/deployments/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar com/mysql/cj/util/TimeZoneMapping.properties
rm com/mysql/cj/util/TimeZoneMapping.properties
rmdir -p com/mysql/cj/util
# End

patch ${JBOSS_HOME}/bin/standalone.sh ${JBOSS_HOME}/patch_standalone.sh
patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly-init-redhat.sh  ${JBOSS_HOME}/patch_wildfly-init-redhat.sh
patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly.conf ${JBOSS_HOME}/patch_wildfly.conf
(cd ${JBOSS_HOME} ; rm patch_standalone.sh patch_wildfly-init-redhat.sh patch_wildfly.conf)

# Check the install by running
#    ~/wildfly/bin/standalone.sh --debug
