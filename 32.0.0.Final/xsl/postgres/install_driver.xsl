<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:jboss="urn:jboss:domain:datasources:7.1"
		xmlns="urn:jboss:domain:datasources:7.1"
		version="2.0">

  <!-- Driver parameters. These should not need to be changed -->
  <xsl:param name="driver_protocol">postgresql</xsl:param>
  <xsl:param name="driver_port">5432</xsl:param>
  <xsl:param name="driver_class">org.postgresql.Driver</xsl:param>
  <xsl:param name="driver_module">org.postgresql</xsl:param>
  <xsl:param name="driver_name">postgresql</xsl:param>

  <xsl:include href="../copy.xsl" />
  <xsl:include href="../dbms/uninstall/driver.xsl" />
  <xsl:include href="../dbms/install/driver.xsl" />

</xsl:stylesheet>
