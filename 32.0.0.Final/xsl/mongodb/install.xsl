<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:jboss="urn:jboss:domain:naming:2.0"
		xmlns="urn:jboss:domain:naming:2.0"
		version="2.0">

  <!-- Binding parameters. These should not need to be changed -->
  <xsl:param name="jndi_name">java:global/MyMongoClient</xsl:param>
  <xsl:param name="mongodb_uri">mongodb+srv://lzuser:mongo_catalog@db-mongodb/</xsl:param>

  <xsl:include href="../copy.xsl" />
  <xsl:include href="../naming/uninstall/object_factory.xsl" />  
  <xsl:include href="../naming/install/object_factory.xsl" />

</xsl:stylesheet>
