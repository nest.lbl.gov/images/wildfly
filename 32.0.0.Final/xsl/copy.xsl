<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0">

  <!-- Declare newline variable -->
  <xsl:variable name="newline">
    <xsl:text>
</xsl:text>
  </xsl:variable>

  <!-- Insert newlines around main element -->
  <xsl:template match="/">
    <xsl:value-of select="$newline"/>
    <xsl:copy>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
    <xsl:value-of select="$newline"/>
  </xsl:template>

  <!-- Copy all parts that are not explicitly changed -->
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
