<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jboss="urn:jboss:domain:naming:2.0" xmlns="urn:jboss:domain:naming:2.0" version="2.0">

  <!-- Removes "object-factory" from configuration -->
  <xsl:template match="jboss:subsystem/jboss:object-factory[@name=$jndi_name]"/>

</xsl:stylesheet>
