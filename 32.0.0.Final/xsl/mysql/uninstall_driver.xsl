<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:jboss="urn:jboss:domain:datasources:7.1"
		xmlns="urn:jboss:domain:datasources:7.1"
		version="2.0">

  <!-- Driver parameters. These should not need to be changed -->
  <xsl:param name="driver_module">org.mysql</xsl:param>

  <xsl:include href="../copy.xsl" />
  <xsl:include href="../dbms/uninstall/driver.xsl" />

</xsl:stylesheet>
