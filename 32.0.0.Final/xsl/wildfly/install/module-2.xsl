<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jboss="urn:jboss:module:1.9" xmlns="urn:jboss:module:1.9" version="2.0">
  <xsl:param name="resource1">database-driver-0.0.0.jar</xsl:param>
  <xsl:param name="resource2">database-driver-0.0.0.jar</xsl:param>
  <xsl:include href="../../copy.xsl"/>
  <!-- Adds "datasource" to configuration -->
  <xsl:template match="jboss:module/jboss:resources/jboss:resource-root">
    <xsl:element name="resource-root">
      <xsl:attribute name="path">
        <xsl:value-of select="$resource1"/>
      </xsl:attribute>
    </xsl:element>
          <xsl:value-of select="$newline"/>
          <xsl:text>        </xsl:text>
    <xsl:element name="resource-root">
      <xsl:attribute name="path">
        <xsl:value-of select="$resource2"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
