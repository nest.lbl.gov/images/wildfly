<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jboss="urn:jboss:domain:datasources:7.1" xmlns="urn:jboss:domain:datasources:7.1" version="2.0">
  <!-- Adds "driver" to configuration -->
  <xsl:template match="jboss:datasources/jboss:drivers/jboss:driver[@module='com.h2database.h2']">
    <xsl:copy-of select="."/>
    <xsl:value-of select="$newline"/>
    <xsl:text>                    </xsl:text>
    <!-- And now write the requested one -->
    <xsl:element name="driver">
      <xsl:attribute name="name">
        <xsl:value-of select="$driver_name"/>
      </xsl:attribute>
      <xsl:attribute name="module">
        <xsl:value-of select="$driver_module"/>
      </xsl:attribute>
      <xsl:value-of select="$newline"/>
      <xsl:text>                        </xsl:text>
      <xsl:element name="driver-class">
        <xsl:value-of select="$driver_class"/>
      </xsl:element>
      <xsl:value-of select="$newline"/>
      <xsl:text>                    </xsl:text>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
