<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jboss="urn:jboss:domain:datasources:7.1" xmlns="urn:jboss:domain:datasources:7.1" version="2.0">
  <!-- Adds "datasource" to configuration -->
  <xsl:template match="jboss:datasources/jboss:datasource[@jndi-name='java:jboss/datasources/ExampleDS']">
    <!-- Copy the ExampleDS -->
    <xsl:copy-of select="."/>
    <xsl:value-of select="$newline"/>
    <xsl:text>                </xsl:text>
    <!-- And now write the requested one -->
    <xsl:element name="datasource">
      <xsl:attribute name="jndi-name">java:jboss/datasources/<xsl:value-of select="$datasource"/></xsl:attribute>
      <xsl:attribute name="pool-name">java:jboss/datasources/<xsl:value-of select="$datasource"/></xsl:attribute>
      <xsl:attribute name="jta">true</xsl:attribute>
      <xsl:attribute name="enabled">true</xsl:attribute>
      <xsl:attribute name="use-java-context">true</xsl:attribute>
      <xsl:attribute name="statistics-enabled">${wildfly.datasources.statistics-enabled:${wildfly.statistics-enabled:false}}</xsl:attribute>
      <xsl:value-of select="$newline"/>
      <xsl:text>                    </xsl:text>
      <xsl:element name="connection-url">jdbc:<xsl:value-of select="$driver_protocol"/>://<xsl:value-of select="$host"/>:<xsl:value-of select="$driver_port"/>/<xsl:value-of select="$database"/>?charSet=UTF8</xsl:element>
      <xsl:value-of select="$newline"/>
      <xsl:text>                    </xsl:text>
      <xsl:element name="driver">
        <xsl:value-of select="$driver_name"/>
      </xsl:element>
      <xsl:value-of select="$newline"/>
      <xsl:text>                    </xsl:text>
      <xsl:element name="security">
        <xsl:attribute name="user-name">
          <xsl:value-of select="$user"/>
        </xsl:attribute>
        <xsl:attribute name="password">
          <xsl:value-of select="$password"/>
        </xsl:attribute>
      </xsl:element>
      <xsl:value-of select="$newline"/>
      <xsl:text>                </xsl:text>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
