<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jboss="urn:jboss:domain:datasources:7.1" xmlns="urn:jboss:domain:datasources:7.1" version="2.0">

  <!-- Removes "datasource" from configuration -->
  <xsl:template match="jboss:datasources/jboss:datasource[@jndi-name=concat('java:jboss/datasources/', $datasource)]"/>

</xsl:stylesheet>
