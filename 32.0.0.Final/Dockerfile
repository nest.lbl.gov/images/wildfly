# First build the database modules for wildfly

# By default, build on JDK 11 (as opposed to 17) on CentOS 7.
ARG jdk_version="jdk11"
ARG wildfly_tag="32.0.0.Final"

FROM quay.io/wildfly/wildfly:${wildfly_tag}-${jdk_version} as modules

USER root
RUN yum install -y patch saxon

RUN mkdir -p modules downloads && chown jboss:jboss modules downloads

USER jboss

ENV SAXON_HOME=/usr/share/java \
    POSTGRES_JDBC_VERSION=42.7.3 \
    MYSQL_CONNECTOR_VERSION=8.3.0 \
    MONGODB_DRIVER_VERSION=5.1.0

COPY --chown=jboss:jboss xml xml
COPY --chown=jboss:jboss xsl xsl

# Create Postgresql module
RUN mkdir -p modules/org/postgresql/main \
    && java -jar ${SAXON_HOME}/saxon.jar \
         -s:xml/postgresql/module.xml \
         -xsl:xsl/wildfly/install/module.xsl \
         -o:modules/org/postgresql/main/module.xml \
         resource=postgresql-${POSTGRES_JDBC_VERSION}.jar \
    && cd modules/org/postgresql/main \
    && curl --insecure -s -L -O https://jdbc.postgresql.org/download/postgresql-${POSTGRES_JDBC_VERSION}.jar

# Create MYSQL module
RUN mkdir -p modules/org/mysql/main \
    && java -jar ${SAXON_HOME}/saxon.jar \
         -s:xml/mysql/module.xml \
         -xsl:xsl/wildfly/install/module.xsl \
         -o:modules/org/mysql/main/module.xml \
         resource=mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar \
    && cd downloads \
    && curl --insecure -s -L -O https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.tar.gz \
    && tar zxvf mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.tar.gz \
        mysql-connector-j-${MYSQL_CONNECTOR_VERSION}/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar \
    && cd .. \
    && mv downloads/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar \
        modules/org/mysql/main

COPY --chown=jboss:jboss patch patch

# Add MDT to MySQL connector
RUN cd downloads && jar xf ../modules/org/mysql/main/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar \
        com/mysql/cj/util/TimeZoneMapping.properties
RUN patch downloads/com/mysql/cj/util/TimeZoneMapping.properties patch/TimeZoneMapping.properties.patch
RUN cd downloads && jar uf ../modules/org/mysql/main/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar \
        com/mysql/cj/util/TimeZoneMapping.properties

# Create MongoDB module
RUN mkdir -p modules/org/mongodb/main \
    && java -jar ${SAXON_HOME}/saxon.jar \
         -s:xml/mongodb/module.xml \
         -xsl:xsl/wildfly/install/module.xsl \
         -o:modules/org/mongodb/main/module.xml \
         resource=mongodb-driver-sync-${MONGODB_DRIVER_VERSION}.jar \
    && cd modules/org/mongodb/main \
    && curl --insecure -s -L -O https://repo1.maven.org/maven2/org/mongodb/bson/${MONGODB_DRIVER_VERSION}/bson-${MONGODB_DRIVER_VERSION}.jar \
    && curl --insecure -s -L -O https://repo1.maven.org/maven2/org/mongodb/bson-record-codec/${MONGODB_DRIVER_VERSION}/bson-record-codec-${MONGODB_DRIVER_VERSION}.jar \
    && curl --insecure -s -L -O https://repo1.maven.org/maven2/org/mongodb/mongodb-driver-core/${MONGODB_DRIVER_VERSION}/mongodb-driver-core-${MONGODB_DRIVER_VERSION}.jar \
    && curl --insecure -s -L -O https://repo1.maven.org/maven2/org/mongodb/mongodb-driver-sync/${MONGODB_DRIVER_VERSION}/mongodb-driver-sync-${MONGODB_DRIVER_VERSION}.jar

#--------
#
# Build a Wildfly image that can be run by any UID/GID
FROM quay.io/wildfly/wildfly:${wildfly_tag}-${jdk_version}

LABEL maintainer="Simon Patton <sjpatton@lbl.gov, Keith Beattie <ksbeattie@lbl.gov>"

# Starting user is "jboss 1000:1000"
# Change to 'root' to install packages and set up sudo.
USER root
RUN yum install -y openssh-clients.x86_64 patch.x86_64 saxon sudo

# Set up sudo to allow for execution of this commands
RUN set -x \
    && mkdir -p /etc/sudoers.d \
    && echo "jboss ALL=(root) NOPASSWD: /map_user" > /etc/sudoers.d/jboss \
    && chmod 440 /etc/sudoers.d/jboss \
    && echo "ghost ALL=(root) NOPASSWD:SETENV: /usr/bin/su - jboss" > /etc/sudoers.d/ghost \
    && echo "ghost ALL=(jboss) NOPASSWD:SETENV: /start_wildfly" >> /etc/sudoers.d/ghost \
    && chmod 440 /etc/sudoers.d/ghost \
    && usermod --shell /bin/bash jboss \
    && chmod 777 /opt

# Copy tha commands for setting up and launching the server.
COPY docker-entrypoint.sh /usr/local/bin/
COPY ./map_user ./start_wildfly ./prepare_standalone_workdir ./populate_standalone_workdir /

# Set the default command to run on boot
# This will boot WildFly in the standalone mode and bind to all interface
ENTRYPOINT ["docker-entrypoint.sh"]

USER jboss

# Install Database modules
RUN mkdir -p ${HOME}/wildfly/modules/org/postgresql/main
COPY --chown=jboss:jboss --from=modules /opt/jboss/modules/org/postgresql/main ${JBOSS_HOME}/modules/org/postgresql/main/
COPY --chown=jboss:jboss --from=modules /opt/jboss/modules/org/mysql/main ${JBOSS_HOME}/modules/org/mysql/main/

ENV JBOSS_EXTRAS=${JBOSS_HOME}/workdir/extras \
    SAXON_HOME=/usr/share/java \
    STANDALONE_WORKDIR=${JBOSS_HOME}/workdir/standalone


# Add convienence link and "extra" directory
RUN ln -s ${JBOSS_HOME} /opt/wildfly \
    && mkdir -p ${JBOSS_EXTRAS} \
    && ln -s ${JBOSS_EXTRAS} ${JBOSS_HOME}/

# Create the readonly copies of the standalone area and set up the links that will be
#   used on the standalone area is populated.
RUN /prepare_standalone_workdir ${JBOSS_HOME} ${STANDALONE_WORKDIR}

COPY --chown=jboss:jboss xsl xsl
COPY --chown=jboss:jboss bin bin
COPY --chown=jboss:jboss patch patch

# Patch the startup files for both maunal and service execution
RUN patch ${JBOSS_HOME}/bin/standalone.sh ${HOME}/patch/standalone.sh.patch \
    && patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly-init-redhat.sh ${HOME}/patch/wildfly-init-redhat.sh.patch \
    && patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly.conf ${HOME}/patch/wildfly.conf.patch

VOLUME [ "/opt/jboss/wildfly/workdir/standalone", \
         "/opt/jboss/wildfly/workdir/extras" ]
