# What is `wildfly`? #

The `wildfly` image is designed to provide a deployable [wildfly release](https://www.wildfly.org/) that has been tailored to support the JEE WAR files available from the [Nest repository](https://gitlab.com/nest.lbl.gov).

The currently supported version of wildfly is [25.0.1.Final](https://issues.redhat.com/secure/ReleaseNote.jspa?projectId=12313721&version=12375434).

At present the tailored features are:

* inclusion of the [postgres JDBC driver](https://jdbc.postgresql.org/);
* inclusion of the [MySQL Connector/J](https://dev.mysql.com/downloads/connector/j/);
* a hook, `local.env`, that is used to set up the local environment in which `wildfly` will execute.
* volume mappings to allow persistent of the `standalone` and `extras` directories.
* the ability to map the `jboss` user in the container onto a host user.
* the `deploy_services` hook to enable images extended from this one to deploy services when the image is run.

The details of how to use these features is documented in the [release's README.md](25.0.1.Final/README.md).


## Creating a new `wildfly` image ##

The [gitlab.com repository](https://gitlab.com/nest.lbl.gov/images/wildfly) for this image is configured to build a new image whenever a tag of the form "XX.YY.ZZ.Final.aaa" is created. This image will be then be available from its [gitlab container registry](https://gitlab.com/nest.lbl.gov/images/wildfly/container_registry).

Of course this does not mean people are not free to clone the repository and do their own `docker build` with the contents.
