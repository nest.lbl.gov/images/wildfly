#
# This file attempts to be the moral equivalent of the Dockerfile, expect
#   it is run to install Wildfly in a running system and installing everything
#   in the user's area.
#

WILDFLY_VERSION=16.0.0.Final
WILDFLY_SHA1=287c21b069ec6ecd80472afec01384093ed8eb7d
JBOSS_HOME=${HOME}/wildfly
POSTGRES_JDBC_VERSION=42.2.5
STANDALONE_WORKDIR=/opt/jboss/workdir/standalone \
JBOSS_EXTRAS=/opt/jboss/workdir/extras \
MYSQL_CONNECTOR_VERSION=5.1.47
MONGO_DRIVER_VERSION=3.10.1
LAUNCH_JBOSS_IN_BACKGROUND=true


cd ${HOME}
curl  --insecure -s -O https://download.jboss.org/wildfly/${WILDFLY_VERSION}/wildfly-${WILDFLY_VERSION}.tar.gz
sha1sum wildfly-${WILDFLY_VERSION}.tar.gz | grep ${WILDFLY_SHA1}
tar xf wildfly-${WILDFLY_VERSION}.tar.gz
mv ${HOME}/wildfly-${WILDFLY_VERSION} ${JBOSS_HOME}
rm wildfly-${WILDFLY_VERSION}.tar.gz


curl  --insecure -s -L -O https://jdbc.postgresql.org/download/postgresql-${POSTGRES_JDBC_VERSION}.jar
mv postgresql-${POSTGRES_JDBC_VERSION}.jar ${JBOSS_HOME}/standalone/deployments/
curl  --insecure -s -L -O https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz
tar zxvf mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz mysql-connector-java-${MYSQL_CONNECTOR_VERSION}/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}-bin.jar
mv mysql-connector-java-${MYSQL_CONNECTOR_VERSION}/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}-bin.jar ${JBOSS_HOME}/standalone/deployments/
rmdir mysql-connector-java-${MYSQL_CONNECTOR_VERSION}
rm -f mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz
curl  --insecure -s -L -O https://oss.sonatype.org/content/repositories/releases/org/mongodb/mongo-java-driver/${MONGO_DRIVER_VERSION}/mongo-java-driver-${MONGO_DRIVER_VERSION}.jar
mv mongo-java-driver-${MONGO_DRIVER_VERSION}.jar ${JBOSS_HOME}/standalone/deployments/

cp patch_standalone.conf patch_standalone.sh patch_wildfly-init-redhat.sh patch_wildfly.conf ${JBOSS_HOME}/
patch ${JBOSS_HOME}/bin/standalone.conf ${JBOSS_HOME}/patch_standalone.conf
patch ${JBOSS_HOME}/bin/standalone.sh ${JBOSS_HOME}/patch_standalone.sh
patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly-init-redhat.sh  ${JBOSS_HOME}/patch_wildfly-init-redhat.sh
patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly.conf ${JBOSS_HOME}/patch_wildfly.conf

# Check the install by running
#    ~/wildfly/bin/standalone.sh --debug
