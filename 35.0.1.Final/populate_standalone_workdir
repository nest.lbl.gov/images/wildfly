#!/bin/bash

# Args:
#     source : the current standalone directory (default: "/opt/wildfly")
#     destination : the root target directory (default: "/opt/wildfly/workdir/standalone")
#     subdir : the subdirectory, if any, below the root target to use (default: nothing)

#
# Each major Wildfly version should have its own subdir, named after its major version
#   number, below the root target directory to avoid unplaned inconsistencies.
# If a directory does not exist it will be created and, if necessary,
#   populated.
# To re-populate a directory simply delete the directory and it will be populated
#   agina next time this command is executed.

SOURCE=${1:-/opt/wildfly}
ROOT_DESTINATION=${2:-/opt/wildfly/workdir/standalone}

# Determine whether a major version subdirectory can be builr
if [ "X" != "X${WILDFLY_VERSION}" ] ; then
    major_version=$(echo ${WILDFLY_VERSION} | cut -d'.' -f1)
fi
if [ "X" == "X${major_version}" ] ; then
    DESTINATION=${ROOT_DESTINATION}
else
    DESTINATION=${ROOT_DESTINATION}/${major_version}
fi

echo "Source=${SOURCE}"
echo "Destination=${DESTINATION}"

if [ ! -d ${DESTINATION}/data ] ; then
    mkdir -p ${DESTINATION}/data
fi

if [ ! -d ${DESTINATION}/log ] ; then
    mkdir -p ${DESTINATION}/log
fi

if [ ! -d ${DESTINATION}/tmp ] ; then
    mkdir -p ${DESTINATION}/tmp
fi

if [ ! -d ${DESTINATION}/configuration ] ; then
    cp -r ${SOURCE}/standalone/configuration.readonly ${DESTINATION}/configuration
    chown jboss ${SOURCE}/standalone/configuration/*
    chmod go-r ${SOURCE}/standalone/configuration/application-roles.properties \
               ${SOURCE}/standalone/configuration/application-users.properties \
               ${SOURCE}/standalone/configuration/mgmt-groups.properties \
               ${SOURCE}/standalone/configuration/mgmt-users.properties
fi

if [ ! -d ${DESTINATION}/deployments ] ; then
    cp -r ${SOURCE}/standalone/deployments.readonly ${DESTINATION}/deployments
    chown jboss ${SOURCE}/standalone/deployments/*
fi
