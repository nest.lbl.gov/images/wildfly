#!/bin/bash
#

# map image user onto host user
echo "Current ID $(id)"
sudo -u root /map_user ${HOST_UID} ${HOST_GID}

if [ "$(whoami)" == "jboss" ] ; then
    /start_wildfly "$@"
else
    sudo -E /usr/bin/su jboss /start_wildfly "$@"
fi
