# This file attempts to be the moral equivalent of the Dockerfile, expect
#   it is run to install Wildfly in a running system and installing everything
#   in the user's area.
#

# Set local  directories
source ${WILDFLY_LOCAL_ENV:-./install.env}

if [ "X" == "X${DOCKER_IMAGE_CLONE}" ] ; then
    echo "The path to the docker_images project must be specified in the DOCKER_IMAGE_CLONE variable" >&2
    exit 1
fi

# Set up environment
export WILDFLY_VERSION=35.0.1.Final
export WILDFLY_SHA1=cd5a99cc776ec8cd4e188a55db115d3747b936ab
export STANDALONE_WORKDIR=${JBOSS_HOME}/workdir/standalone
export JBOSS_EXTRAS=${JBOSS_HOME}/workdir/extras
export JBOSS_WORKDIR=${JBOSS_HOME}/downloads
export SAXON_HE_VERSION=12-5
export POSTGRES_JDBC_VERSION=42.7.5
export MYSQL_CONNECTOR_VERSION=9.2.0
export MONGODB_DRIVER_VERSION=5.3.0

here=$(pwd)
cd ${HOME}
curl --insecure -L -O https://github.com/wildfly/wildfly/releases/download/${WILDFLY_VERSION}/wildfly-${WILDFLY_VERSION}.tar.gz
case "`uname`" in
    Darwin*)
        shasum wildfly-${WILDFLY_VERSION}.tar.gz | grep ${WILDFLY_SHA1}
        result=${?}
        ;;

    *)
        sha1sum wildfly-${WILDFLY_VERSION}.tar.gz | grep ${WILDFLY_SHA1}
        result=${?}
        ;;

esac
if [ 0 != ${result} ] ; then
    exit 2
fi
tar -xf wildfly-${WILDFLY_VERSION}.tar.gz
rm -fr ${JBOSS_HOME}
mv wildfly-${WILDFLY_VERSION} ${JBOSS_HOME}
rm wildfly-${WILDFLY_VERSION}.tar.gz

# Prepare to create DB modules
mkdir -p ${XSL_HOME}
cp -r ${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/xsl/* ${XSL_HOME}
mkdir -p ${XSL_SCRIPTS}
cp ${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/bin/*install* ${XSL_SCRIPTS}
mkdir -p ${JBOSS_WORKDIR} ${SAXON_HOME} ${XSL_HOME}
(cd ${JBOSS_WORKDIR} && curl --insecure -L -O https://github.com/Saxonica/Saxon-HE/releases/download/SaxonHE${SAXON_HE_VERSION}/SaxonHE${SAXON_HE_VERSION}J.zip)
unzip -d ${JBOSS_WORKDIR}  ${JBOSS_WORKDIR}/SaxonHE${SAXON_HE_VERSION}J.zip
mv ${JBOSS_WORKDIR}/saxon-he-[0-9\.]*.jar ${SAXON_HOME}/saxon.jar
mv ${JBOSS_WORKDIR}/lib ${SAXON_HOME}/
cp -rp ${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/xsl ${USER_HOME}/
cp ${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/bin/* ${JBOSS_HOME}/bin/

# Create Postgresql module
mkdir -p ${JBOSS_HOME}/modules/org/postgresql/main
java -jar ${SAXON_HOME}/saxon.jar \
         -s:${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/xml/postgresql/module.xml \
         -xsl:${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/xsl/wildfly/install/module.xsl \
         -o:${JBOSS_HOME}/modules/org/postgresql/main/module.xml \
         resource=postgresql-${POSTGRES_JDBC_VERSION}.jar
(cd ${JBOSS_HOME}/modules/org/postgresql/main && curl --insecure -L -O https://jdbc.postgresql.org/download/postgresql-${POSTGRES_JDBC_VERSION}.jar)

# Create MYSQL module
mkdir -p ${JBOSS_HOME}/modules/org/mysql/main
java -jar ${SAXON_HOME}/saxon.jar \
         -s:${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/xml/mysql/module.xml \
         -xsl:${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/xsl/wildfly/install/module.xsl \
         -o:${JBOSS_HOME}/modules/org/mysql/main/module.xml \
         resource=mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar
(cd ${JBOSS_WORKDIR} && curl --insecure -L -O https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.tar.gz)
(cd ${JBOSS_WORKDIR} && tar zxvf mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.tar.gz \
        mysql-connector-j-${MYSQL_CONNECTOR_VERSION}/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar)
mv ${JBOSS_WORKDIR}/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar \
        ${JBOSS_HOME}/modules/org/mysql/main

# Add MDT to MySQL connector
(cd ${JBOSS_WORKDIR} && jar xf ../modules/org/mysql/main/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar \
        com/mysql/cj/util/TimeZoneMapping.properties)
patch ${JBOSS_WORKDIR}/com/mysql/cj/util/TimeZoneMapping.properties ${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/patch/TimeZoneMapping.properties.patch
(cd ${JBOSS_WORKDIR} && jar uf ../modules/org/mysql/main/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar \
        com/mysql/cj/util/TimeZoneMapping.properties)

# Create MongoDB module
mkdir -p ${JBOSS_HOME}/modules/org/mongodb/main
java -jar ${SAXON_HOME}/saxon.jar \
         -s:${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/xml/mongodb/module.xml \
         -xsl:${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/xsl/wildfly/install/module-2.xsl \
         -o:${JBOSS_HOME}/modules/org/mongodb/main/module.xml \
         resource1=mongodb-driver-core-${MONGODB_DRIVER_VERSION}.jar \
         resource2=mongodb-driver-sync-${MONGODB_DRIVER_VERSION}.jar
(cd ${JBOSS_HOME}/modules/org/mongodb/main && curl --insecure -s -L -O https://repo1.maven.org/maven2/org/mongodb/bson/${MONGODB_DRIVER_VERSION}/bson-${MONGODB_DRIVER_VERSION}.jar)
(cd ${JBOSS_HOME}/modules/org/mongodb/main && curl --insecure -s -L -O https://repo1.maven.org/maven2/org/mongodb/bson-record-codec/${MONGODB_DRIVER_VERSION}/bson-record-codec-${MONGODB_DRIVER_VERSION}.jar)
(cd ${JBOSS_HOME}/modules/org/mongodb/main && curl --insecure -s -L -O https://repo1.maven.org/maven2/org/mongodb/mongodb-driver-core/${MONGODB_DRIVER_VERSION}/mongodb-driver-core-${MONGODB_DRIVER_VERSION}.jar)
(cd ${JBOSS_HOME}/modules/org/mongodb/main && curl --insecure -s -L -O https://repo1.maven.org/maven2/org/mongodb/mongodb-driver-sync/${MONGODB_DRIVER_VERSION}/mongodb-driver-sync-${MONGODB_DRIVER_VERSION}.jar)

patch ${JBOSS_HOME}/bin/standalone.sh ${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/patch/standalone.sh.patch
patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly-init-redhat.sh ${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/patch/wildfly-init-redhat.sh.patch
patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly.conf ${DOCKER_IMAGE_CLONE}/${WILDFLY_VERSION}/patch/wildfly.conf.patch

rm -fr ${JBOSS_WORKDIR}

cd ${here}

# Check the install by running
#    ~/wildfly/bin/standalone.sh --debug
