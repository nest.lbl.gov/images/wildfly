<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jboss="urn:jboss:domain:naming:2.0" xmlns="urn:jboss:domain:naming:2.0" version="2.0">
  <!-- Adds "binding" to configuration -->
  <xsl:template match="jboss:subsystem/jboss:remote-naming">
    <xsl:element name="bindings">
        <xsl:value-of select="$newline"/>
        <xsl:text>                </xsl:text>
      <xsl:element name="object-factory">
        <xsl:attribute name="name">
          <xsl:value-of select="$jndi_name"/>
        </xsl:attribute>
        <xsl:attribute name="module">org.mongodb</xsl:attribute>
        <xsl:attribute name="class">com.mongodb.client.MongoClientFactory</xsl:attribute>
        <xsl:value-of select="$newline"/>
        <xsl:text>                  </xsl:text>
        <xsl:element name="environment">
          <xsl:value-of select="$newline"/>
          <xsl:text>                      </xsl:text>
          <xsl:element name="property">
            <xsl:attribute name="name">connectionString</xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="$mongodb_uri"/>
            </xsl:attribute>
          </xsl:element>
          <xsl:value-of select="$newline"/>
          <xsl:text>                  </xsl:text>
        </xsl:element>
        <xsl:value-of select="$newline"/>
        <xsl:text>              </xsl:text>
      </xsl:element>
      <xsl:value-of select="$newline"/>
      <xsl:text>              </xsl:text>
    </xsl:element>
    <xsl:value-of select="$newline"/>
    <xsl:text>            </xsl:text>
    <xsl:copy>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
