<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:jboss="urn:jboss:domain:datasources:7.2"
		xmlns="urn:jboss:domain:datasources:7.2"
		version="2.0">

  <!-- Datatsource parameters -->
  <xsl:param name="datasource">ServiceDS</xsl:param>
  <xsl:param name="host">postgres_host</xsl:param>
  <xsl:param name="database">database</xsl:param>
  <xsl:param name="user">jboss</xsl:param>
  <xsl:param name="password">change_me</xsl:param>

  <xsl:include href="install_driver.xsl" />
  <xsl:include href="../dbms/uninstall/datasource.xsl" />
  <xsl:include href="../dbms/install/datasource.xsl" />

</xsl:stylesheet>
