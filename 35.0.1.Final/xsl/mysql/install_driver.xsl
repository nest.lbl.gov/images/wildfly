<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:jboss="urn:jboss:domain:datasources:7.2"
		xmlns="urn:jboss:domain:datasources:7.2"
		version="2.0">

  <!-- Driver parameters. These should not need to be changed -->
  <xsl:param name="driver_protocol">mysql</xsl:param>
  <xsl:param name="driver_port">3306</xsl:param>
  <xsl:param name="driver_class">com.mysql.cj.jdbc.Driver</xsl:param>
  <xsl:param name="driver_module">org.mysql</xsl:param>
  <xsl:param name="driver_name">mysql</xsl:param>

  <xsl:include href="../copy.xsl" />
  <xsl:include href="../dbms/uninstall/driver.xsl" />
  <xsl:include href="../dbms/install/driver.xsl" />

</xsl:stylesheet>
