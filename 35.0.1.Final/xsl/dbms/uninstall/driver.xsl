<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jboss="urn:jboss:domain:datasources:7.2" xmlns="urn:jboss:domain:datasources:7.2" version="2.0">

  <!-- Removes "driver" from configuration -->
  <xsl:template match="jboss:datasources/jboss:drivers/jboss:driver[@module=$driver_module]"/>

</xsl:stylesheet>
