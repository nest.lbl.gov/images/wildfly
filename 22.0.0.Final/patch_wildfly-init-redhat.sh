*** wildfly/docs/contrib/scripts/init.d/wildfly-init-redhat.sh	1969-12-21 17:00:00.000000000 -0700
--- wildfly/docs/contrib/scripts/init.d/wildfly-init-redhat.sh	1969-12-21 17:00:00.000000000 -0700
***************
*** 154,159 ****
--- 154,161 ----
  		cat /dev/null > "$JBOSS_CONSOLE_LOG"
  		currenttime=$(date +%s%N | cut -b1-13)
  
+ 	if [ ! -z "$JBOSS_USER" -a "`whoami`" != "$JBOSS_USER" ]; then
+ 
  		if [ "$JBOSS_MODE" = "standalone" ]; then
  			cd $JBOSS_HOME >/dev/null 2>&1
  			daemon --user=$JBOSS_USER --pidfile=$JBOSS_PIDFILE LAUNCH_JBOSS_IN_BACKGROUND=1 JBOSS_PIDFILE=$JBOSS_PIDFILE "$JBOSS_SCRIPT -c $JBOSS_CONFIG $JBOSS_OPTS &" >> $JBOSS_CONSOLE_LOG 2>&1
***************
*** 164,169 ****
--- 166,176 ----
  			cd - >/dev/null 2>&1
  		fi
  
+ 	else
+ 		bash -c "LAUNCH_JBOSS_IN_BACKGROUND=1 JBOSS_PIDFILE=$JBOSS_PIDFILE $JBOSS_SCRIPT -c $JBOSS_CONFIG" 2>&1 > $JBOSS_CONSOLE_LOG &
+ 	fi
+ 
+ 
  		count=0
  		until [ $count -gt $STARTUP_WAIT ]
  		do
